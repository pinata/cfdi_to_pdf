defmodule CfdiToPdf do
  alias CfdiToPdf.XmlReader
  require EEx

  EEx.function_from_file(:def, :create_html, (__ENV__.file |> Path.dirname |> Path.join("./templates/invoice.html.eex")), [:invoice])
  EEx.function_from_file(:def, :javascripts, (__ENV__.file |> Path.dirname |> Path.join("./templates/javascripts.html.eex")), [])
  EEx.function_from_file(:def, :stylesheets, (__ENV__.file |> Path.dirname |> Path.join("./templates/stylesheets.html.eex")), [])


  @moduledoc "Module to convert a CFDI in XML format to a PDF file"
  def xml_to_invoice(xml) do
    parsed_map = XmlReader.parse_xml_to_invoice(xml)
    Map.merge(%CfdiToPdf.Invoice{}, parsed_map)
  end

  def html_to_pdf(html_file, pdf_file) do
    Porcelain.exec("wkhtmltopdf", ["-q", "--margin-top" ,"10",
      "--margin-bottom", "20", "--margin-left", "10", "--orientation",
      "portrait", "--page-size", "letter", "--zoom", "0.8",
      html_file, pdf_file])
  end

  def convert(xml, pdf_file) do
    invoice = xml_to_invoice(xml)
    html = create_html(invoice)
    html_file = "invoice.html"
    File.write!(html_file, html)
    html_to_pdf(html_file, pdf_file)
    File.rm!("invoice.html")
  end
end
