defmodule CfdiToPdf.Fiscal do
  @moduledoc "Fiscal information of a CFDI"
  defstruct id: nil, series: nil, folio: nil, datetime: nil,
    place: nil, signing_datetime: nil, sender_certificate: nil,
    sat_certificate: nil, payment_information: nil, version: nil
end
