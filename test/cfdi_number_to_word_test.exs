defmodule CfdiNumberToWordTest do
  use ExUnit.Case

  test "convert 0 to word" do
    number = 0
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "cero pesos 00/100"
  end

  test "convert 6 to word" do
    number = "6"
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "seis pesos 00/100"
  end

  test "convert 10 to word" do
    number = 10
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "diez pesos 00/100"
  end

  test "convert 12 to word" do
    number = '12'
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "doce pesos 00/100"
  end

  test "convert 20 to word" do
    number = 20
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "veinte pesos 00/100"
  end

  test "convert 63 to word" do
    number = "63"
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "sesenta y tres pesos 00/100"
  end

  test "convert 88.9 to word" do
    number = 88.9
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "ochenta y ocho pesos 9/100"
  end

  test "convert 55.85 to word" do
    number = 55.85
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "cincuenta y cinco pesos 85/100"
  end

  test "convert 100 to word" do
    number = '100'
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "cien pesos 00/100"
  end

  test "convert 200 to word" do
    number = '200'
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "doscientos pesos 00/100"
  end

  test "convert 125.689 to word" do
    number = "125.689"
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "ciento veinticinco pesos 68/100"
  end

  test "convert 1000 to word" do
    number = 1000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "mil pesos 00/100"
  end

  test "convert 1248 to word" do
    number = 1248
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "mil doscientos cuarenta y ocho pesos 00/100"
  end

  test "convert 68540 to word" do
    number = 68540
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "sesenta y ocho mil quinientos cuarenta pesos 00/100"
  end

  test "convert 780000 to word" do
    number = 780000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "setecientos ochenta mil pesos 00/100"
  end

  test "convert 11000.11000 to word" do
    number = 11000.11000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "once mil pesos 11/100"
  end

  test "convert 999999 to word" do
    number = 999999
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "novecientos noventa y nueve mil novecientos noventa y nueve pesos 00/100"
  end

  test "convert 1000000 to word" do
    number = 1000000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "un millon pesos 00/100"
  end

  test "convert 999999999 to word" do
    number = 999999999
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "novecientos noventa y nueve millones novecientos noventa y nueve mil novecientos noventa y nueve pesos 00/100"
  end

  test "convert 1784561 to word" do
    number = 1784561
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "un millon setecientos ochenta y cuatro mil quinientos sesenta y uno pesos 00/100"
  end

  test "convert 27845124.27845124 to word" do
    number = 27845124.27845124
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "veintisiete millones ochocientos cuarenta y cinco mil ciento veinticuatro pesos 27/100"
  end

  test "convert 297949949 to word" do
    number = 297949949
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "doscientos noventa y siete millones novecientos cuarenta y nueve mil novecientos cuarenta y nueve pesos 00/100"
  end

  test "convert 5745186 to word" do
    number = 5745186
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "cinco millones setecientos cuarenta y cinco mil ciento ochenta y seis pesos 00/100"
  end

  test "convert 1000000000 to word" do
    number = 1000000000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "mil millones pesos 00/100"
  end

  test "convert 1578654212 to word" do
    number = 1578654212
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "mil quinientos setenta y ocho millones seiscientos cincuenta y cuatro mil doscientos doce pesos 00/100"
  end

  test "convert 24578469532 to word" do
    number = 24578469532
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "veinticuatro mil quinientos setenta y ocho millones cuatroscientos sesenta y nueve mil quinientos treinta y dos pesos 00/100"
  end

  test "convert 999999999999 to word" do
    number = 999999999999
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "novecientos noventa y nueve mil novecientos noventa y nueve millones novecientos noventa y nueve mil novecientos noventa y nueve pesos 00/100"
  end

  test "convert 17693.290000 to word" do
    number = 17693.290000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "diecisiete mil seiscientos noventa y tres pesos 29/100"
  end

  test "convert 451548566265 to word" do
    number = 451548566265
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "cuatroscientos cincuenta y un mil quinientos cuarenta y ocho millones quinientos sesenta y seis mil doscientos sesenta y cinco pesos 00/100"
  end

  test "convert 10000000000 to word" do
    number = 10000000000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "diez mil millones pesos 00/100"
  end

  test "convert 100000000000 to word" do
    number = 100000000000
    number_in_words = CfdiToPdf.NumberToWord.to_word(number)
    assert number_in_words == "cien mil millones pesos 00/100"
  end

end
