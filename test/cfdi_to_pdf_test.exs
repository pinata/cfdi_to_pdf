defmodule CfdiToPdfTest do
  use ExUnit.Case

  test "convert xml to pdf" do
    xml = File.read!(Path.expand("./test/cfdi.xml"))
    pdf = Path.expand("./test/cfdi.pdf")
    CfdiToPdf.convert(xml, pdf)
  end

  test "convert and xml with local taxes to pdf" do
    xml = File.read!(Path.expand("./test/cfdi_local_taxes.xml"))
    pdf = Path.expand("./test/cfdi_local_taxes.pdf")
    CfdiToPdf.convert(xml, pdf)
  end

  test "convert and xml with no concepts table pdf" do
    xml = File.read!(Path.expand("./test/cfdi_with_no_concepts_table.xml"))
    invoice =  CfdiToPdf.xml_to_invoice(xml)
    html = CfdiToPdf.create_html(invoice)
    File.write!(Path.expand("./test/cfdi_with_no_concepts_table.html"), html)
    pdf = Path.expand("./test/cfdi_with_no_concepts_table.pdf")
    CfdiToPdf.convert(xml, pdf)
  end

  test "convert and xml with retentions to pdf" do
    xml = File.read!(Path.expand("./test/cfdi_with_isr_retention.xml"))
    pdf = Path.expand("./test/cfdi_with_isr_retention.pdf")
    CfdiToPdf.convert(xml, pdf)
  end

  test "convert an html file to a pdf file" do
    html = Path.expand("./test/cfdi.html")
    pdf = Path.expand("./test/cfdi.pdf")
    CfdiToPdf.html_to_pdf(html, pdf)
  end

  test "convert a xml to an html file" do
    xml = File.read!(Path.expand("./test/cfdi.xml"))
    invoice =  CfdiToPdf.xml_to_invoice(xml)
    html = CfdiToPdf.create_html(invoice)
    File.write!(Path.expand("./test/cfdi.html"), html)
  end

  test "parse xml to CfdiToPdf.Invoice" do
    xml = File.read!(Path.expand("./test/cfdi.xml"))
    CfdiToPdf.xml_to_invoice(xml)
    true
  end

  # test "generate a html from invoice struct" do
  #   invoice = %CfdiToPdf.Invoice{
  #     sender: %CfdiToPdf.Person{rfc: "XAXX010101000",
  #       name: "Fulano de tal SA de CV", city: "Chihuahua",
  #       state: "Chihuahua", neighborhood: "Villa Bonita",
  #       street: "Calle Segunda 201", zip_code: 31000,
  #       regime: "Persona Fisica con Actividad Empresarial"},
  #     recipient: %CfdiToPdf.Person{rfc: "XAXX010101000",
  #       name: "Mangano de Tal SA de CV", city: "Chihuahua",
  #       state: "Chihuahua", neighborhood: "Villa Bonita",
  #       street: "Calle Segunda 201", zip_code: 31000,
  #       regime: "Persona Fisica con Actividad Empresarial"},
  #     fiscal: %CfdiToPdf.Fiscal{ id: "6441D77E-7E57-4BB0-B5FD-A15EF29A7930",
  #       series: "FS", folio: 4,
  #       datetime: "lunes, 14 de septiembre de 2015 23:18",
  #       place: "Chihuahua, Chihuahua",
  #       sender_certificate: "00001000000305493329",
  #       sat_certificate: "20001000000100005867",
  #       signing_datetime: "2015-09-14T18:18:44",
  #       version: "3.2",
  #       payment_information: "Deposito en Cuenta /"},
  #     signing: %CfdiToPdf.Signing{
  #       cfdi: "xU4srD0BAJ0n2LK4INY9LGJB5FCv4BJiBxVAH+QsYxoI1/7SITNPzZ1fN0"<>
  #         "+MDJa1HlWD0l0PANkWMxadYO+tiQxhknogPllozh/WKB7mtHm+t4e4GvXkdFMek"<>
  #         "NwaxrH5g5cEHAuuyKRCXCF+2RHyBSl9tP1aZIje1I+2xh1AcoY=",
  #       sat: "d0BKPDAVNbZpAMC0VOupAwXjr7hDQAQ8cdq+NT+8bACrwEfV4u3Dw9v3b3LbN"<>
  #         "IW4wvWG46uPS+1cimm+lZ/xnZKcyIz4HGCK70linVkVc4lQmWSUaCpEowtaN9Jt1p"<>
  #         "tNm+gKcO3FOuaoygHSnqt6xbHq0BufbeHI3iBo/SUahDw=",
  #       original: "||1.0|6441D77E-7E57-4BB0-B5FD-A15EF29A7930|2015-09-14T"<>
  #         "18:18:44|xU4srD0BAJ0n2LK4INY9LGJB5FCv4BJiBxVAH+QsYxoI1/7SITNPzZ1"<>
  #         "fN0+MDJa1HlWD0l0PANkWMxadYO+tiQxhknogPllozh/WKB7mtHm+t4e4GvXkdFM"<>
  #         "ekNwaxrH5g5cEHAuuyKRCXCF+2RHyBSl9tP1aZIje1I+2xh1AcoY=|2000100000"<>
  #         "0100005867||"},
  #     totals: %CfdiToPdf.Totals{
  #           taxable: "40.00", subtotal: "40.00",
  #           iva: "6.40", total: "46.40",
  #           total_with_words: "**********************DOCE MIL NOVECIENTOS "<>
  #             "OCHENTA Y TRES PESOS 13/100 M.N.**********************"
  #     },
  #     items: [
  #       %CfdiToPdf.Item{
  #         quantity: 1, code: "003", unit: "Kg", description: "Arena para gato",
  #           unit_price: "40.00", amount: "40.00"
  #       }
  #     ]
  #   }
  #   _html = CfdiToPdf.create_html(invoice)
  #   true
  # end
end
