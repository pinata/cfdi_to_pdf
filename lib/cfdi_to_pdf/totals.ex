defmodule CfdiToPdf.Totals do
  @moduledoc "All subtotal total and taxes information"
  defstruct total: "", iva: "", taxable: "", subtotal: "",
    total_with_words: ""
end
