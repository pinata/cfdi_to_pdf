defmodule CfdiToPdf.Signing do
  @moduledoc "Signing and certificate information"
  defstruct cfdi: "", sat: "", original: "", uuid: "", date: "",
    sat_number: ""
end
