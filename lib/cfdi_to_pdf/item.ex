defmodule CfdiToPdf.Item do
  @moduledoc "Information for one item in the invoice"
  defstruct quantity: 0, code: "", unit: "", description: "",
    unit_price: 0, amount: 0
end
