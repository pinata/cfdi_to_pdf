defmodule CfdiToPdf.Invoice do
  @moduledoc "Invoice representation"
  defstruct cfdi_type: "", sender: nil, recipient: nil, fiscal: nil, signing: nil,
    totals: nil, items: [], locals: [], retentions: [], forwards: []
end
