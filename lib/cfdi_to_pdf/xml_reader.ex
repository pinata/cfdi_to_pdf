defmodule CfdiToPdf.XmlReader do
  @moduledoc "Reads CFDI in XML format"
  import SweetXml

  def parse_xml_to_invoice(doc) do
    doc |> xpath(
      ~x"/cfdi:Comprobante",
      cfdi_type: ~x"/cfdi:Comprobante/@tipoDeComprobante",
      sender: [
        ~x"/cfdi:Comprobante/cfdi:Emisor",
        rfc: ~x"@rfc",
        name: ~x"@nombre",
        regime: ~x"cfdi:RegimenFiscal/@Regimen",
        street: ~x"cfdi:DomicilioFiscal/@calle",
        number: ~x"cfdi:DomicilioFiscal/@noExterior",
        neighborhood: ~x"cfdi:DomicilioFiscal/@colonia",
        city: ~x"cfdi:DomicilioFiscal/@ciudad",
        state: ~x"cfdi:DomicilioFiscal/@estado",
        zip_code: ~x"cfdi:DomicilioFiscal/@codigoPostal",
      ],
      recipient: [
        ~x"/cfdi:Comprobante/cfdi:Receptor",
        rfc: ~x"@rfc",
        name: ~x"@nombre",
        regime: ~x"cfdi:RegimenFiscal/@Regimen",
        street: ~x"cfdi:Domicilio/@calle",
        number: ~x"cfdi:Domicilio/@noExterior",
        neighborhood: ~x"cfdi:Domicilio/@colonia",
        city: ~x"cfdi:Domicilio/@ciudad",
        state: ~x"cfdi:Domicilio/@estado",
        zip_code: ~x"cfdi:Domicilio/@codigoPostal",
      ],
      fiscal: [
        ~x"/cfdi:Comprobante",
        id: ~x"cfdi:Complemento/tfd:TimbreFiscalDigital/@UUID",
        series: ~x"@serie",
        version: ~x"@version",
        folio: ~x"@folio",
        datetime: ~x"@fecha",
        place: ~x"@LugarExpedicion",
        currency: ~x"@Moneda",
        signing_datetime:
          ~x"cfdi:Complemento/tfd:TimbreFiscalDigital/@FechaTimbrado",
        sender_certificate: ~x"@noCertificado",
        sat_certificate:
          ~x"cfdi:Complemento/tfd:TimbreFiscalDigital/@noCertificadoSAT",
        payment_information: ~x"@metodoDePago",
      ],
      totals: [
        ~x"/cfdi:Comprobante",
        total: ~x"@total",
        subtotal: ~x"@subTotal",
        total_with_words:
        ~x"cfdi:Addenda/if:DocumentoInterfactura/if:Encabezado/@importeConLetra"
      ],
      signing: [
        ~x"/cfdi:Comprobante/cfdi:Complemento/tfd:TimbreFiscalDigital",
        sat: ~x"@selloSAT",
        cfdi: ~x"@selloCFD",
        uuid: ~x"@UUID",
        date: ~x"@FechaTimbrado",
        sat_number: ~x"@noCertificadoSAT",
      ],
      items: [
        ~x"/cfdi:Comprobante/cfdi:Conceptos/cfdi:Concepto"l,
        quantity: ~x"@cantidad",
        description: ~x"@descripcion",
        unit: ~x"@unidad",
        unit_price: ~x"@valorUnitario",
        amount: ~x"@importe",
      ],
      locals: [
        ~x"/cfdi:Comprobante/cfdi:Complemento/implocal:ImpuestosLocales/implocal:RetencionesLocales"l,
        tax_name: ~x"@ImpLocRetenido",
        tax_rate: ~x"@TasadeRetencion",
        tax_total: ~x"@Importe"
      ],
      forwards: [
        ~x"/cfdi:Comprobante/cfdi:Impuestos/cfdi:Traslados/cfdi:Traslado"l,
        tax_name: ~x"@impuesto",
        tax_rate: ~x"@tasa",
        tax_total: ~x"@importe"
      ],
      retentions: [
        ~x"/cfdi:Comprobante/cfdi:Impuestos/cfdi:Retenciones/cfdi:Retencion"l,
        tax_name: ~x"@impuesto",
        tax_total: ~x"@importe"
      ]
    )
  end
end
