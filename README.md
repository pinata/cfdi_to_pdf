CfdiToPdf
=========

?re=CSL981002U60&rr=GOER860507DY3&tt=540.11000000&id=343b6d31-7d4b-469e-e00438455062

Extrae los siguientes campos:

Emisor
* RFC Emisor
* Regimen Fiscal
* RFC Receptor

Folio Fiscal
* Folio Fiscal
* No. Serie del CSD
* Lugar y fecha de emisión

Conceptos
* Lista de Conceptos
  * Cantidad
  * Unidad de Medida
  * Descripción
  * Precio Unitario
  * Importe

Totales  
* Subtotal
* IVA
* ISR Retenido
* IVA Retenido
* Total
* Total en letra

Pago
* Método de Pago
* Forma de Pago

Sellos
* Sello digital del CFDI
* Sello del SAT
* Cadena Original del Complemento de certificación digital del SAT
* No de Serie de Certificado del SAT
* Fecha y hora de certificación
