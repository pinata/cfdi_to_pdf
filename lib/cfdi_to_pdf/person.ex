defmodule CfdiToPdf.Person do
  @moduledoc "Person information"
  defstruct rfc: "", name: "", street: "",
            neighborhood: "", city: "", state: "",
            zip_code: 0, regime: ""
end
