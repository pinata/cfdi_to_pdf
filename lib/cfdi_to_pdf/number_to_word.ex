defmodule CfdiToPdf.NumberToWord do
  @moduledoc "Convert a number into a word"
  @singular_currency "peso"
  @plural_currency "pesos"
  @max_number 999_999_999_999
  @units ~w(cero uno dos tres cuatro cinco seis siete ocho nueve)
  @tens ~w(diez once doce trece catorce quince dieciseis diecisiete dieciocho diecinueve)
  @ten_ten ~w(cero diez veinte treinta cuarenta cincuenta sesenta setenta ochenta noventa)
  @hundreds ~w(_ ciento doscientos trescientos cuatroscientos quinientos seiscientos setecientos ochocientos novecientos)

  def to_word(number) do
    cond do
      number |> is_float() ->
        integer_number = trunc(number)
        fractional_part = to_string(number) |> String.split(["."]) |>
        Enum.at(1) |> String.slice(0..1)
      number |> is_integer() ->
        integer_number = number
        fractional_part = "0"
      number |> is_binary() or number |> is_list() ->
        integer_number = to_string(number) |> Integer.parse() |> elem(0)
        fractionals = to_string(number) |> String.split(["."])
        case Enum.count(fractionals) do
          1 ->
            fractional_part = "0"
          2 ->
            fractional_part = fractionals |> Enum.at(1) |> String.slice(0..1)
        end
    end
    if integer_number > @max_number do
      raise "Supported number: [- +]999.999.999.999"
    end

    fractional_words = to_string(fractional_part) |> String.slice(0..1)

    if fractional_part == "0" do
      fractional_words = "00/100"
    else
      fractional_words = fractional_part <> "/100"
    end

    #if fractional_part > 0, do: fractional_words = "  #{to_word(fractional_part)}", else: fractional_words = ""

    number_to_word =
    cond do
      Enum.member?(0..99, integer_number) -> read_dozens(integer_number)
      Enum.member?(100..999, integer_number) -> read_hundreds(integer_number)
      Enum.member?(1000..999999, integer_number) -> read_thousands(integer_number)
      Enum.member?(1000000..999999999, integer_number) -> read_million(integer_number)
      Enum.member?(1000000000..999999999999, integer_number) -> read_billions(integer_number)
    end
    formatted_word = number_to_word |> String.replace("_", " ") |> String.replace(~r/\s+/, " ")
    |> String.strip() |> String.replace("uno mil", "un mil")
    formatted_word <> " pesos " <> fractional_words
  end

  def read_dozens(number) do
   if number < 10 do
     Enum.at(@units, number)
   else
     ten = Integer.digits(number) |> Enum.at(0)
     unit = Integer.digits(number) |> Enum.at(1)

     cond do
       Enum.member?(10..19, number) -> Enum.at(@tens, unit)
       Enum.member?(21..29, number) -> "veinti#{Enum.at(@units, unit)}"
       true -> Enum.at(@ten_ten, ten) <> (if unit > 0, do: " y #{Enum.at(@units, unit)}", else: "")
     end
   end
  end

  def read_hundreds(number) do
    if number == 100 do
      "cien"
    else
      hundred = div(number, 100)
      ten = rem(number, 100)

      Enum.at(@hundreds, hundred) <> (if ten > 0, do: " #{read_dozens(ten)}", else: "")
    end
  end

  def read_thousands(number) do
    thousand = div(number, 1000)
    hundred = rem(number, 1000)

    thousand_to_word =
    cond do
      Enum.member?(2..9, thousand) -> Enum.at(@units, thousand)
      Enum.member?(10..99, thousand) -> read_dozens(thousand)
      Enum.member?(100..999, thousand) -> read_hundreds(thousand)
      true -> ""
    end <> " mil "

    if hundred > 0, do: thousand_to_word <> read_hundreds(hundred), else: thousand_to_word
  end

  def read_million(number) do
    million = div(number, 1000000)
    thousand = rem(number, 1000000)

    millon_to_word =
    cond do
      million == 1 -> " un millon "
      Enum.member?(2..9, million) -> Enum.at(@units, million)
      Enum.member?(10..99, million) -> read_dozens(million)
      Enum.member?(100..999, million) -> read_hundreds(million)
      true -> ""
    end
    unless million == 1, do: millon_to_word = millon_to_word <> " millones "

    thousand_words =
    cond do
      Enum.member?(1..999, thousand) -> read_hundreds(thousand)
      Enum.member?(1000..999999, thousand) -> read_thousands(thousand)
      true -> ""
    end
    millon_to_word <> thousand_words
  end

  def read_billions(number) do
    billion = div(number, 1000000)
    million = rem(number, 1000000)
    "#{read_thousands(billion)} #{read_million(million)}"
  end
end
